resource "mongodbatlas_project" "project" {
    name   = var.project_name
    org_id = var.org_id
    project_owner_id = var.project_owner_id
}

output "project_name" {
    value = mongodbatlas_project.project.name
}

resource "mongodbatlas_cluster" "cluster" {
    project_id   = mongodbatlas_project.project.id
    name         = var.cluster_name
    mongo_db_major_version = var.mongodbversion
    cluster_type = "REPLICASET"

    replication_specs {
        num_shards = 1
        regions_config {
            region_name     = var.region
            electable_nodes = 3
            priority        = 7
            read_only_nodes = 0
        }
    }

    cloud_backup = true
    auto_scaling_disk_gb_enabled = true

    # Provider Settings "block"
    provider_name               = var.cloud_provider
    disk_size_gb                = 100
    provider_instance_size_name = "M0"
}

resource "mongodbatlas_database_user" "user" {
    project_id   = mongodbatlas_project.project.id
    username = var.dbuser
    password = var.dbuser_password
    auth_database_name = "admin"

    roles {
        role_name = "readWrite"
        database_name = var.database_name
    }

    labels {
        key = "Name"
        value = "DB User1"
    }
}

resource "mongodbatlas_project_ip_access_list" "ip" {
    project_id = mongodbatlas_project.project.id
    ip_address = var.ip_address
    comment = "IP Address for access(cluster)"
}
